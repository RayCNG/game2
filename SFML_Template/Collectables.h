#pragma once
#include<SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Network.hpp>
#include <SFML/Audio.hpp>
#include <iostream>
#include <cmath>

enum  CollectiblesType
{
	DEFALUT=0, DAMAGING, HEALING, NROFTYPES
};

class Collectables
{

private:
	sf::CircleShape item;

	int type; 

	void initItem(const sf::RenderWindow* window);

public:
	Collectables(const sf::RenderWindow* window,int type);
	~Collectables();

	//Accessors
	const sf::CircleShape& getCollectiblesShape() const;
	const int& getType() const; 
	
	void update();
	void render(sf::RenderTarget* target);
};

