#include "Collectables.h"



void Collectables::initItem(const sf::RenderWindow* window)
{
	//Beh�ver en static_cast f�r att g�ra om (rand()%10+10) till en float.
	//(rand()%10+10) �r egentligen en int. 

	this->item.setRadius(static_cast<float>(rand() % 10 + 10));
	sf::Color color;
	
	switch (this->type)
	{
	case DEFALUT:
		color = sf::Color(rand() % 255 + 1, rand() % 255 + 1, rand() % 255 + 1);
		break;
	case DAMAGING:
		color = sf::Color::Red;
		this->item.setOutlineColor(sf::Color::White);
		this->item.setOutlineThickness(2.f);
		break;
	case HEALING:
		color = sf::Color::Green;
		this->item.setOutlineColor(sf::Color::White);
		this->item.setOutlineThickness(2.f);
		break;
	}
	
	
	
	this->item.setFillColor(color);
	this->item.setPosition(
		sf::Vector2f(
		static_cast<float>(rand() %window->getSize().x), 
		static_cast<float>(rand() %window->getSize().y)
		)
	);
}	

Collectables::Collectables(const sf::RenderWindow* window,int type)
	: type(type)
{
	this->initItem(window);
}


Collectables::~Collectables()
{
}

const sf::CircleShape & Collectables::getCollectiblesShape() const
{
	return this->item;
}

const int & Collectables::getType() const
{
	return this->type;
}

void Collectables::update()
{
}

void Collectables::render(sf::RenderTarget * target)
{
	//I videon gjorde han Rendertarget till en referens ist�ller f�r en pointer
	target->draw(this->item);
}
