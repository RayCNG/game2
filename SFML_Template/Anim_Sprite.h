#pragma once
#include<SFML/Graphics.hpp>
#include<iostream>
#include <vector>

class Anim_Sprite
{
public:
	Anim_Sprite(std::string file_Path, float anim_Speed);
	~Anim_Sprite();

	

private:
	void init_Variables(std::string file_Path, float anim_Speed);
	
	sf::Texture Sprite_Texture;
	sf::Sprite Sprite;
	std::string file_Path;
	

	float anim_Speed;

	std::vector<Animation> Animations;


};

class Animation
{
public:
	Animation(sf::Texture & Texture);
	void create_Anim_Frame(sf::IntRect Coords, int frame_Position);

private:
	std::vector<sf::IntRect> AnimationCointainer;
};



