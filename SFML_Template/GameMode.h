#pragma once
#include<iostream>
#include<ctime>
#include<vector>
#include<sstream>
#include"Player.h"
#include"Enemy.h"
#include"Collectables.h"



class GameMode
{

private:
	sf::VideoMode videoMode;
	sf::RenderWindow* window;
	sf::Event sfmlEvent; 

	bool endgame;

	Player Player;

	//Change this, right now only one Enemy
	Enemy Enemy;

	std::vector<Collectables> CollectableItems;

	float spawnTimerMax;
	float spawnTimer;
	int maxCollectables; 

	int points;

	sf::Font font;
	sf::Text text;
	sf::Text endText;
	
	//Initializers
	void initVariables(); 
	void initWindow(); 
	void initFonts();
	void initText();

public:
	//Constructors and deconstructors
	GameMode();
	~GameMode();

	//Accessors
	const bool& getEndGame() const;


	//Mofifiers

	//Functionns
	const bool running() const; 
	void pollEvent();
	
	
	void spawnCollectables(); 
	const int randomizeType()const;

	void updatePlayer();
	void updateEnemy();
	void updateGUI();
	void updateCollision();
	void update();

	void renderGUI(sf::RenderTarget* target);
	void render();
};

