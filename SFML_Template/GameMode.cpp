#include "GameMode.h"

//Constructors and Deconstructors
GameMode::GameMode()
{
	this->initVariables();
	this->initWindow();
	this->initFonts();
	this->initText();
}

GameMode::~GameMode()
{
	delete this->window;
}

const bool & GameMode::getEndGame() const
{
	return this->endgame;
}

//Initializers
void GameMode::initVariables()
{
	this->endgame = false; 
	this->spawnTimerMax = 10.f;
	this->spawnTimer = this->spawnTimerMax;
	this->maxCollectables = 10; 
	this->points = 0;
}

void GameMode::initWindow()
{
	this->videoMode = sf::VideoMode(800, 600);
	
	this->window = new sf::RenderWindow(this->videoMode, "Game 1", sf::Style::Close | sf::Style::Titlebar);
	this->window->setFramerateLimit(60);
}

void GameMode::initFonts()
{
	if (!this->font.loadFromFile("Fonts/Minecraft.ttf"))
	{
		std::cout << "! ERROR::GAME::INITFONTS::UNABLE TO LOAD Minecraft.ttf";
	}


}

void GameMode::initText()
{
	this->text.setFont(this->font);
	this->text.setFillColor(sf::Color::White);
	this->text.setCharacterSize(23);

	this->endText.setFont(font);
	this->endText.setFillColor(sf::Color::Red);
	this->endText.setCharacterSize(60);
	this->endText.setString("YOU ARE DEAD SUCKER!\nExit the game");
	this->endText.setPosition(sf::Vector2f(40, 200));



}


//Functions
const bool GameMode::running() const
{
	return this->window->isOpen(); //&& this->getEndGame() == false;
}

//Handels the window events
void GameMode::pollEvent()
{
	while (this->window->pollEvent(this->sfmlEvent))
	{
		switch (this->sfmlEvent.type)
		{
		case sf::Event::Closed:
			this->window->close(); 
			break;

		case sf::Event::KeyPressed:
			if (this->sfmlEvent.key.code == sf::Keyboard::Escape)
				this->window->close();
				break;
			
		}
	}
}

void GameMode::spawnCollectables()
{
	//timer
	if (this->spawnTimer < this->spawnTimerMax)
		this->spawnTimer += 1.f;
	else
	{
		if (this->CollectableItems.size() < this->maxCollectables)
		{
			//Baser p� en enum gjord i Collectables NROFTYPES �r den sista i Enum classen vilket g�r att
			//vi kan f� ut hur m�nga typer som finns. 
			this->CollectableItems.push_back(Collectables(this->window,this->randomizeType()));
			this->spawnTimer = 0.f;
		}
	}
}

const int GameMode::randomizeType() const
{
	int type = CollectiblesType::DEFALUT;
	int randomValue = rand() % 100 + 1;
	if (randomValue >80 && randomValue <= 90)
	{
		type = CollectiblesType::DAMAGING;
	}
	else if (randomValue >90 && randomValue <= 100)
	{
		type = CollectiblesType::HEALING;
	}

	return type;
}

void GameMode::updatePlayer()
{
	this->Player.update(this->window);
	if (this->Player.getHP() <= 0)
	{
		this->endgame = true;
	}
}

void GameMode::updateEnemy()
{
	this->Enemy.update(&Player);

}

void GameMode::updateGUI()
{
	std::stringstream ss;
	ss << "Points " << this->points<<"\n"
		<<" Health: "<<this->Player.getHP()<<" / "<<this->Player.getHPMax();
	this->text.setString(ss.str());

}

void GameMode::updateCollision()
{
	//Check the Player Collision
	for (size_t i = 0; i < this->CollectableItems.size(); i++)
	{
		if (this->Player.getPlayerSprite().getGlobalBounds().intersects(this->CollectableItems[i].getCollectiblesShape().getGlobalBounds()))
		{
			switch (CollectableItems[i].getType())
			{
			case CollectiblesType::DEFALUT:
				//Add points
				this->points++;
				break;
				
			case CollectiblesType::DAMAGING:
				Player.takeDamage(1);
				break;
				
			case CollectiblesType::HEALING:
				Player.gainHealth(1);
				
			default:
				break;
			}

			
			
			//Remove the Collactible
			//Begin �r f�r att ta fram de f�rsta elementet i Vectorn,
			//Variabeln i plussas p� f�r att ta reda p� vilken collectable det �r i Vectorn/arrayen. 
			this->CollectableItems.erase(this->CollectableItems.begin() + i);
			
		}
	}
	
}

void GameMode::update()
{
	this->pollEvent();
	//if false, Pauses the game and displays the endText.
	if (!endgame)
	{
		this->updatePlayer();
		this->updateEnemy();
		this->spawnCollectables();
		this->updateCollision();
		this->updateGUI();
	}
	
}

void GameMode::renderGUI(sf::RenderTarget* target)
{
	target->draw(this->text);
	if (this->endgame == true)
	{
		target->draw(this->endText);
	}
}

void GameMode::render()
{
	this->window->clear();

	//render stuff
	this->Player.render(this->window);
	this->Enemy.render(this->window);
	
	//for each element in ColletableItems Vector render the element.
	//auto f�r att anpassa objekttypen efter det som h�mtas i CollectableItems. 
	for (auto i : this->CollectableItems)
	{
		i.render(this->window);
	}

	this->renderGUI(this->window);

	this->window->display();
}
