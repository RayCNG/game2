#include "Enemy.h"
#define PI 3.14159265


Enemy::Enemy()
{
	this->init_Variables();
	this->init_Enemy();
	
}

//Constructor
Enemy::~Enemy()
{
}

//Accessors
const int & Enemy::get_getHp() const
{
	return this->e_Hp;
}

const int & Enemy::get_AttackPower() const
{
	return this->e_AttackPower;
}

const float & Enemy::get_Speed() const
{
	return this->e_Speed;
}

float Enemy::calculate_RangeFromPlayer(Player * player)
{	

	float distance_to_Player =sqrtf(
		powf(this->e_Sp.getPosition().x - player->getPlayerSprite().getPosition().x, 2.0)
		+
		powf(this->e_Sp.getPosition().y - player->getPlayerSprite().getPosition().y, 2.0));
	
	
	if (distance_to_Player < 100.f)
		this->IsPlayerFound = true;
	else
		this->IsPlayerFound = false; 
		
	return distance_to_Player;
}

void Enemy::update_Enemy_State()
{
	//Set a State based on the rotation of the enemy and the player
	if (this->e_Rotation >= -45 && this->e_Rotation <= 45)
		this->e_State = e_RIGHT;
	if (this->e_Rotation >= 45 && this->e_Rotation <= 135)
		this->e_State = e_DOWN;
	if (this->e_Rotation >= 135 || this->e_Rotation <= -135)
		this->e_State = e_LEFT;
	if (this->e_Rotation >= -135 && this->e_Rotation <= -45)
		this->e_State = e_TOP;


//Update the Enemy Sprite
	switch (this->e_State)
	{
	case e_LEFT:
		this->e_Sp.setTextureRect(sf::IntRect(32 * 3, 0, 32, 32));
		break;
	case e_RIGHT:
		this->e_Sp.setTextureRect(sf::IntRect(32 * 2, 0, 32, 32));
		break;
	case e_TOP:
		this->e_Sp.setTextureRect(sf::IntRect(32 * 0, 0, 32, 32));
		break;
	case e_DOWN:
		this->e_Sp.setTextureRect(sf::IntRect(32 * 1, 0, 32, 32));
		break;

	default:
		break;
	}
	
}


void Enemy::chase_Player(Player* player)
{
	if (IsPlayerFound == true)
	{
		// angle (in radians) between monster and player
		float angle = atan2(player->getPlayerSprite().getPosition().y - this->e_Sp.getPosition().y,
			player->getPlayerSprite().getPosition().x - this->e_Sp.getPosition().x);

		//Set the Angle
		//TODO Set a state based on the rotation outcome.
		this->e_Rotation = angle * 180 / PI;
		std::cout << angle*180/PI<<"\n";

		// monster.speed is the amount of pixels to move
		// If this doesn't work, invert cos for x and sin for y
		this->e_Sp.move(cos(angle) * this->e_Speed, sin(angle) * this->e_Speed);
		
		
	}
}

void Enemy::update(Player* player)
{
	calculate_RangeFromPlayer(player);
	chase_Player(player);
	update_Enemy_State();
	
}

void Enemy::render(sf::RenderTarget * target)
{
	target->draw(this->e_Sp);
}

void Enemy::init_Enemy()
{
	//Texture
	if (!this->e_Tx.loadFromFile("Sprites/Enemy.png"))
	{
		std::cout << "! ERROR Failed to load Sprite/Enemy.png";
	}
	this->e_Sp.setTexture(this->e_Tx);
	this->e_Sp.setPosition(this->e_Position);


}

void Enemy::init_Variables()
{
	
	this->e_MaxHp = 2;
	this->e_Hp = this->e_MaxHp;
	this->e_Speed = 1.f;
	this->e_Rotation = 0.f;
	this->e_Position = sf::Vector2f(100,100);

}
