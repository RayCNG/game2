#include "Player.h"


//Constructors and Deconstructors
Player::Player()
{
	this->initVariables();
	this->initPlayer();
}
Player::~Player()
{

}

//Accessors
const sf::RectangleShape & Player::getPlayerShape() const
{
	return this->playerShape;
}
const int & Player::getHP() const
{
	return this->hp;
}
const int & Player::getHPMax() const
{
	return this->hpMax;
}

const int & Player::getPlayerState() const
{
	return this->PlayerState;
}

const sf::Texture & Player::getPlayerTexture() const
{
	return this->playerTx;
}

const sf::Sprite & Player::getPlayerSprite() const
{
	return this->playerSp;
}

const sf::Vector2f & Player::getPlayerPos() const
{
	return this->playerSp.getPosition();
}

//initializers
void Player::initShape()
{
	this->movementSpeed = 5.f;
	this->playerShape.setFillColor(sf::Color::Green);
	this->playerShape.setSize(sf::Vector2f(50.f, 50.f));
}

void Player::initVariables()
{
	this->movementSpeed = 5.f;
	this->hpMax = 10;
	this->hp = hpMax;
	this->p_Rotation = sf::Vector2f(0, 0);
	this->IsMoving = false; 
	
}

void Player::initPlayer()
{
	
	if (!this->playerTx.loadFromFile("Sprites/Player.png"))
	{
		std::cout << "! ERROR FAILED TO LOAD Sprites/Player.png";
	}
	
	this->playerSp.setTexture(this->playerTx);

}

void Player::takeDamage(const int damage)
{
	if (this->hp >0)
		this->hp -= damage;

	if (this->hp<0)	
		this->hp = 0;
}

void Player::gainHealth(const int health)
{
	if (this->hp < this->hpMax)
		this->hp += health;
	if (this->hp > this->hpMax)
		this->hp = this->hpMax;
}


//Functions
void Player::updateInput()
{
	//window bounds collisiton

//Keyboard input, Update Shapes.
	//Left
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		this->playerSp.move(-this->movementSpeed, 0.f);
		this->IsMoving = true;
		this->PlayerState = LEFT;

	}
	//Right
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		this->playerSp.move(this->movementSpeed, 0.f);
		this->IsMoving = true;
		this->PlayerState = RIGHT;
	}
	//Top
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		this->playerSp.move(0.f, -this->movementSpeed);
		this->IsMoving = true;
		this->PlayerState = TOP;
	}
	//Down
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		this->playerSp.move(0.f, this-> movementSpeed);
		this->IsMoving = true;
		this->PlayerState = DOWN;
	}
	else
	{
		this->IsMoving = false; 
	}
}

void Player::updatePlayer()
{
	sf::Time time = Clock.getElapsedTime();
	
	std::cout << time.asSeconds()<<"\n";
	switch (this->PlayerState)
	{
	case LEFT:
		this->playerSp.setTextureRect(sf::IntRect(32 * 3, 0, 32, 32));
		if (IsMoving == true)
		{
			//TODO make animations, prefered not in runtime... Make a animation Class?
			

		}
		break;
	case RIGHT:
		this->playerSp.setTextureRect(sf::IntRect(32 * 2, 0, 32, 32));
		break;
	case TOP:
		this->playerSp.setTextureRect(sf::IntRect(32 * 0, 0, 32, 32));
		break;
	case DOWN:
		this->playerSp.setTextureRect(sf::IntRect(32 * 1, 0, 32, 32));
		break;

	default:
		break;
	}
}

void Player::updateWindowBoundsCollision(const sf::RenderTarget * target)
{
	//Left
	if (this->playerSp.getGlobalBounds().left <= 0.f)
		this->playerSp.setPosition(0.f, this->playerSp.getGlobalBounds().top);

	//Right
	if (this->playerSp.getGlobalBounds().left + this->playerSp.getGlobalBounds().width >= target->getSize().x)
		this->playerSp.setPosition(target->getSize().x - this->playerSp.getGlobalBounds().width, this->playerSp.getGlobalBounds().top);
	
	//Top
	if (this->playerSp.getGlobalBounds().top <= 0.f)
		this->playerSp.setPosition(this->playerSp.getGlobalBounds().left, 0.f);

	//Bottom
	if (this->playerSp.getGlobalBounds().top + this->playerSp.getGlobalBounds().height >= target->getSize().y)
		this->playerSp.setPosition(this->playerSp.getGlobalBounds().left, target->getSize().y - this->playerSp.getGlobalBounds().height);
}

void Player::update(const sf::RenderTarget* target)
{
	
	this->updateInput();
	this->updatePlayer();
	this->updateWindowBoundsCollision(target);
	
}

void Player::render(sf::RenderTarget * target)
{
	target->draw(this->playerSp);
}
