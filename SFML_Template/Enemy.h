#pragma once
#include "Player.h"
#include <cmath>
#include <iostream>
#include<ctime>

enum ENEMY_STATE { e_LEFT,e_RIGHT,e_TOP,e_DOWN,e_NUOFSTATES };
class Enemy
{
public:
	//Constructors and Deconstructors
	Enemy();
	~Enemy();

	//Accessors
	
	const int& get_getHp() const;
	const int& get_AttackPower() const;
	const float& get_Speed()const; 


	//Functions

	float calculate_RangeFromPlayer(Player* player);
	void chase_Player(Player* player);
	void update_Enemy_State();
	

	void update(Player* player);
	void render(sf::RenderTarget* target);
	

private:
	sf::Texture e_Tx;
	sf::Sprite e_Sp;

	//Variables
	int e_Hp;
	int e_MaxHp;
	int e_AttackPower;
	float e_Speed;

	bool IsPlayerFound;
	bool IsWalking;

	float e_Rotation;
	sf::Vector2f e_Position;

	int e_State;

	//initializers
	void init_Enemy();
	void init_Variables();

	
};

