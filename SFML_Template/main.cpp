#include "GameMode.h"



int main()
{
	//initialize random seed
	srand(static_cast<unsigned>(time(0)));

	//Initialize GameMode Object
	GameMode GameMode;

	//Gameloop
	while (GameMode.running())
	{
		GameMode.update();
		GameMode.render();
	}

	return 0;
}
