#pragma once
#include"Collectables.h"



enum PlayerState{LEFT,RIGHT,TOP,DOWN,NUOFSTATES};

class Player
{
private:
	sf::RectangleShape playerShape;
	sf::Texture playerTx;
	sf::Sprite playerSp;
	sf::Clock Clock;
	sf::Time Time; 
	

	sf::Vector2f p_Rotation;
	float movementSpeed;
	int PlayerState;
	int hp;
	int hpMax;

	float anim_Speed;
	float anim_Time; 

	bool IsMoving;

	//Initializers
	void initShape();
	void initVariables(); 
	void initPlayer(); 


public:
	//Constructor and Deconstructor
	Player();
	~Player();
	
	//Accessors
	const sf::RectangleShape& getPlayerShape() const;
	const int& getHP() const; 
	const int& getHPMax() const;
	const int& getPlayerState()const;
	const sf::Texture& getPlayerTexture() const;
	const sf::Sprite& getPlayerSprite() const;
	const sf::Vector2f& getPlayerPos() const;
	

	//Functions
	void takeDamage(const int damage);
	void gainHealth(const int health);
	void updateInput();
	void updatePlayer();
	void updateWindowBoundsCollision(const sf::RenderTarget* target);
	void update(const sf::RenderTarget* target);
	void render(sf::RenderTarget* target); 
};

